<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateTable extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '134217728');
        ini_set('max_execution_time', 300000);
    }

    public function run()
    {

        $result = [
            'status' => 'failed',
            'tables' => [],
            'error_message' => ''
        ];

        $queries = [
            'purchases' => [
                'control' => "SHOW TABLES LIKE 'purchases'",
                'create' => "CREATE TABLE `purchases` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `status` varchar(255) DEFAULT NULL,
                    `message` varchar(255) DEFAULT NULL,
                    `expire_date` datetime DEFAULT NULL,
                    `uid` varchar(255) DEFAULT NULL,
                    `receipt` varchar(255) DEFAULT NULL,
                    `client_token` varchar(255) DEFAULT NULL,
                    `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
                    `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
                    PRIMARY KEY (`id`)
                  ) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;"
            ],
            'devices' => [
                'control' => "SHOW TABLES LIKE 'devices'",
                'create' => "CREATE TABLE `devices` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `uid` varchar(255) DEFAULT NULL,
                    `appid` varchar(255) DEFAULT NULL,
                    `language` varchar(255) DEFAULT NULL,
                    `os` varchar(255) DEFAULT NULL,
                    `client_token` varchar(255) DEFAULT NULL,
                    `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
                    `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
                    PRIMARY KEY (`id`)
                  ) ENGINE=InnoDB AUTO_INCREMENT=656 DEFAULT CHARSET=latin1;"
            ],
        ];

        foreach ($queries as $key => $query) {
            try {
                $control_result = $this->db->query($query['control']);

                if ($control_result->result_id->num_rows) {
                    $result['tables'][] = [
                        'table_name' => $key,
                        'message' => 'this table is excist'
                    ];
                } else {
                    if ($this->db->query($query['create'])) {
                        $result['tables'][] = [
                            'table_name' => $key,
                            'message' => 'this table is created'
                        ];
                    } else {
                        $result['tables'][] = [
                            'table_name' => $key,
                            'message' => 'table create failed'
                        ];
                    }
                }
                $result['status'] = 'success';
            } catch (Exception $e) {
                $result['error_message'] = 'database error';
                echo json_encode($result);
                exit();
            }
        }

        echo json_encode($result);
    }
}
