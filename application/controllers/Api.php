<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '134217728');
        ini_set('max_execution_time', 300000);
    }

    public function register($same_user = 0)
    {
        $user_device_data = $this->generateUserDevice(['name' => 'register', 'static_user' => $same_user]);
        $full_user_device_data = $user_device_data;
        unset($user_device_data['client_token']);

        $response_data = $full_user_device_data['client_token'];

        try {
            $query = $this->db->where($user_device_data)->get('devices');

            if (!$query->result_id->num_rows) {
                $this->db->insert('devices', $full_user_device_data);
            } else {
                $response_data = $query->result_array()[0]['client_token'];
            }
        } catch (Exception $e) {
            echo json_encode(['status' => 'false', 'message' => 'database error']);
            exit();
        }

        echo json_encode(['status' => 'true', 'register' => 'OK', 'client_token' => $response_data]);
    }

    public function purchase($same_user = 0)
    {
        $user_device_data = $this->generateUserDevice(['name' => 'purchase', 'static_user' => $same_user]);

        $result = [
            'status' => 'false',
            'message' => 'google or ios not verify user'
        ];

        if ($this->googleIos($user_device_data['receipt'])) {
            $full_user_device_data = $user_device_data;
            unset($user_device_data['client_token']);
            unset($user_device_data['receipt']);
            $expire_date_object = new DateTime('now', new DateTimeZone('-0600'));

            try {

                $query = $this->db->where($user_device_data)->get('devices');

                if ($query->result_id->num_rows) {
                    $result['status'] = 'true';
                    $result['message'] = 'OK';
                    $result['expire_date'] = $expire_date_object->format('Y-m-d H:i:s');
                    $result['uid'] = $full_user_device_data['uid'];
                    $result['receipt'] = $full_user_device_data['receipt']; /* not sure return response */
                    $result['client_token'] = $full_user_device_data['client_token'];
                } else {
                    $result['message'] = 'not registered user';
                }

                $this->db->insert('purchases', $result);
            } catch (Exception $e) {
                echo json_encode(['status' => 'false', 'message' => 'database error']);
                exit();
            }
        }

        echo json_encode($result);
    }

    public function checkSubscription($client_token = 0)
    {
        /*sample token*/
        /*85cbfbb654804108f0ac4cb6d40c5510*/
        $return = [
            'status' => 'success',
            'subscription' => 'failed',
            'message' => ''
        ];

        if (!ctype_alnum($client_token)) {
            $return['message'] = 'only letters and digits';
        } else {
            $query = $this->db->where('client_token', $client_token)->get('devices');
            if ($query->result_id->num_rows) {
                $return['subscription'] = 'success';
                $return['message'] = 'subscribed user';
            } else {
                $result['message'] = 'not subscribed user';
            }
        }
        echo json_encode($return);
    }

    private function googleIos($hash_string)
    {
        $return = false;
        $last_character = substr($hash_string, -1);
        if (is_numeric($last_character) and $last_character % 2) {
            $return = true;
        }
        return $return;
    }

    private function generateUserDevice($user_type)
    {
        $return_user_device_data = [];

        if ($user_type['name'] == 'register') {

            $static_user_device_data = [
                'uid' => '44eaf935-663e-475e-ab77-5f93fcb0ba6f',
                'appid' => '1e27cd81-24ea-4464-95fb-d39212314a91',
                'language' => 'tr',
                'os' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36',
                'client_token' => '85cbfbb654804108f0ac4cb6d40c5510'
            ];

            if (!$user_type['static_user']) {
                $return_user_device_data['uid'] = $this->generateToken();
                $return_user_device_data['appid'] = $this->generateToken();
                $return_user_device_data['language'] = $this->generateLang();
                $return_user_device_data['os'] = $this->generateOs();
                $return_user_device_data['client_token'] = $this->generateToken();
            } else {
                $return_user_device_data = $static_user_device_data;
            }
        }

        if ($user_type['name'] == 'purchase') {

            $static_user_device_data = [
                'uid' => '44eaf935-663e-475e-ab77-5f93fcb0ba6f',
                'appid' => '1e27cd81-24ea-4464-95fb-d39212314a91',
                'language' => 'tr',
                'os' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36',
                'client_token' => '85cbfbb654804108f0ac4cb6d40c5510',
                'receipt' => hash('md5', $this->generateToken())
            ];

            if (!$user_type['static_user']) {
                $return_user_device_data['uid'] = $this->generateToken();
                $return_user_device_data['appid'] = $this->generateToken();
                $return_user_device_data['language'] = $this->generateLang();
                $return_user_device_data['os'] = $this->generateOs();
                $return_user_device_data['client_token'] = $this->generateToken();
                $return_user_device_data['receipt'] = hash('md5', $this->generateToken());
            } else {
                $return_user_device_data = $static_user_device_data;
            }
        }

        return $return_user_device_data;
    }

    private function generateOs()
    {
        $os_list = [
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_5_0 rv:5.0) Gecko/20180527 Firefox/37.0',
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/5311 (KHTML, like Gecko) Chrome/40.0.832.0 Mobile Safari/5311',
            'Mozilla/5.0 (X11; Linux x86_64; rv:6.0) Gecko/20150731 Firefox/37.0',
            'Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10_6_2 rv:6.0; sl-SI) AppleWebKit/531.38.3 (KHTML, like Gecko) Version/5.0 Safari/531.38.3',
            'Opera/8.68 (X11; Linux i686; en-US) Presto/2.11.238 Version/12.00',
            'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.01; Trident/4.0)'
        ];

        return $os_list[rand(0, 5)];
    }

    private function generateLang()
    {
        $os_list = [
            'tr',
            'en',
            'fr',
            'de',
            'gr',
            'ru'
        ];

        return $os_list[rand(0, 5)];
    }

    private function generateToken()
    {
        return bin2hex(random_bytes(16));
    }
}
