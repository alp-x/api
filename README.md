# Api

Api server.

## Installation

Use the git to install foobar.

```bash
git clone https://alp-x@bitbucket.org/alp-x/api.git
```

## Install and Usage

```php
/*step 1*/
install xampp this version
https://www.apachefriends.org/xampp-files/7.4.24/xampp-windows-x64-7.4.24-2-VC15-installer.exe
no extra setting for xamp
/*step 1*/

/*step 2*/
git clone https://alp-x@bitbucket.org/alp-x/api.git
/*step 2*/

/*step 3*/
create necessary table, go to this link, if not create please manuelly create table
http://localhost/createTable/run
/*step 3*/

setup is finish.


# returns
# for a random user
'{"status":"true","register":"OK","client_token":"xxx"}'
http://localhost/api/register

/*-------------------------------------*/

# returns
# for a static user
'{"status":"true","register":"OK","client_token":"xxx"}'
http://localhost/api/register/1

/*-------------------------------------*/

# returns
# for a random user
'{"status":"false","message":"not registered user"}'
or
'{"status":"false","message":"google or ios not verify user"}'
http://localhost/api/purchase

/*-------------------------------------*/

# returns
# for a static user
'{"status":"false","message":"google or ios not verify user"}'
or
'{"status":"true","message":"OK","expire_date":"2021-10-10 04:58:00","uid":"44eaf935-663e-475e-ab77-5f93fcb0ba6f","receipt":"26a4fd2cd8f381be0fce934dc33afc59","client_token":"85cbfbb654804108f0ac4cb6d40c5510"}'
http://localhost/api/purchase/1

/*-------------------------------------*/

# returns
'{"status":"success","subscription":"success","message":"subscribed user"}'
or
'{"status":"success","subscription":"failed","message":""}'
http://localhost/api/checkSubscription/85cbfbb654804108f0ac4cb6d40c5510

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
alp-x